
function CheckInternetConnection {
    return Test-Connection "www.mghelpme.com" -Count 1 -Quiet
}

function UpdatePortable {
    param (
        $Drive
    )

    if (CheckInternetConnection) {
        Set-Location "$($Drive):\Genesis\"
        # Update Portable
        Write-Host "Updating Portable Genesis..." -ForegroundColor Yellow
        Start-Process "PortableGit\git-cmd.exe" -ArgumentList "git add . && git reset --hard && git pull && exit" -Wait
    } else {
        Write-Host "No internet connection... Could not update portable Genesis... Proceeding to install current version..." -ForegroundColor Yellow
    }
    
}

function InstallFromPortable {
    param (
        $Drive
    )

    UpdatePortable -Drive $Drive
    Set-Location "$($Drive):\"
    Write-Host "Copying files... This can take a while..." -ForegroundColor Yellow
    try {
        Copy-Item -Path "\Genesis" -Destination 'C:\1 MG\Genesis\' -Recurse
        Write-Host "Files copied sucessfully!" -ForegroundColor Green
    } catch {
        Write-Host "Error Copying Files: $($Error[0])" -ForegroundColor Red
    }

}

function InstallFromInternet {
    Start-Process "\Genesis\PortableGit\git-cmd.exe" -ArgumentList "git add . && git reset --hard && git pull && git clone https://bitbucket.org/WillBixler/Genesis/Genesis.git `"C:\1 MG\Genesis`" && exit" -Wait
}

function CheckInstallation {
    if (Get-Item "C:\1 MG\Genesis" -Force -ErrorAction SilentlyContinue ) {
        return $true
    } else {
        return $false
    }
}

Clear-Host

Write-Host "Checking Genesis Installation" -ForegroundColor Yellow
if (!(CheckInstallation)) {
    # Genesis not installed in 1 MG
    Write-Host "No Genesis installation found... Attempting to install..." -ForegroundColor Yellow
    Get-Volume | ForEach-Object {
        # Loop through each drive to find portable Genesis
        try {
            $drive = $_.DriveLetter
            Set-Location "$($drive):\Genesis" -ErrorAction Stop
            # Genesis is in current drive
            Write-Host "Found Portable Genesis in $($drive):" -ForegroundColor Green
            InstallFromPortable -Drive $drive
            Write-Host "Portable Install Finished" -ForegroundColor Yellow
        } catch {
            # Didnt Find Genesis in current drive
        }
    }
    if (CheckInstallation) {
        Write-Host "Genesis installed sucessfully from portable!" -ForegroundColor Green
    } else {
        Write-Host "Installation from portable failed... Attempting to install from internet..." -ForegroundColor Red
        InstallFromInternet -Drive $drive
        
        if (CheckInstallation) {
            Write-Host "Installation from internet successful!" -ForegroundColor Green
        } else {
            Write-Host "Could not install Genesis... ABORTING!" -ForegroundColor Red
            Pause
            Exit -hard
        }
    }
} else {
    Write-Host "Genesis found on local disk" -ForegroundColor Green
}

Write-Host "Done Installing Genesis!" -ForegroundColor Green