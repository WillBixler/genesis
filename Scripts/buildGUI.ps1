
Clear-Host
Write-Host "Building GUI" -ForegroundColor Yellow

Add-Type -assembly System.Windows.Forms

$ScriptPath = Split-Path $MyInvocation.InvocationName

$buttonSize = New-Object System.Drawing.Size(200, 50)
$buttonFont = New-Object System.Drawing.Font("Arial",12,[System.Drawing.FontStyle]::Regular)

$inProgressColor = [System.Drawing.Color]::LightYellow
$completeColor = [System.Drawing.Color]::LightGreen

$main_form = New-Object System.Windows.Forms.Form
$main_form.Text ='Genesis'
$main_form.Width = 400
$main_form.Height = 200
$main_form.AutoSize = $true
$main_form.Padding = 20
$main_form.FormBorderStyle = "FixedSingle"
$main_form.MaximizeBox = $false
$main_form.MinimizeBox = $false

$logo = [System.Drawing.Image]::Fromfile("$($GenesisLocation)\Resources\MG Logo.png")
$logoBox = New-Object Windows.Forms.PictureBox
$logoBox.Width = $logo.Size.Width
$logoBox.Height = $logo.Size.Height
$logoBox.Image = $logo
$logoBox.Location = New-Object System.Drawing.Size(200, 20)

$buttonsGroup = New-Object System.Windows.Forms.GroupBox
$buttonsGroup.Location = New-Object System.Drawing.Size(20,160)
$buttonsGroup.AutoSize = $true
$buttonsGroup.Padding = 20

$advancedButtons = New-Object System.Windows.Forms.GroupBox
$advancedButtons.Location = New-Object System.Drawing.Size(20,120)
$advancedButtons.AutoSize = $true
$advancedButtons.Padding = 20
$advancedButtons.Text = "Advanced"

$runButton = New-Object System.Windows.Forms.Button
$runButton.Location = New-Object System.Drawing.Size(20,20)
$runButton.Size = New-Object System.Drawing.Size(640, 50)
$runButton.Text = "Run Full Genesis"
$runButton.Font = $buttonFont
$runButton.Add_Click( {
    $runButton.Text = "Running Full Genesis..."
    $runButton.BackColor = $inProgressColor
    if ($fNameInput.Text -and $lNameInput.Text) {
        $uName = "$($fNameInput.Text)$($lNameInput.Text)"
    } else {
        $uName = $false
    }
    Invoke-Expression "& '$($GenesisLocation)\Scripts\fullGenesis.ps1' -builtByMG $($builtByMGCheckBox.Checked) -uName '$($uName)'"
    $runButton.Text = "Run Full Genesis"
    $runButton.BackColor = $completeColor
} )

$builtByMGCheckBox = New-Object System.Windows.Forms.Checkbox
$builtByMGCheckBox.Location = New-Object System.Drawing.Size(20,70)
$builtByMGCheckBox.Size = $buttonSize
$builtByMGCheckBox.Text = "Built by Millennium Group"
$builtByMGCheckBox.Font = $buttonFont

$fNameLabel = New-Object System.Windows.Forms.Label
$fNameLabel.Location = New-Object System.Drawing.Size(240, 75)
$fNameLabel.Size = New-Object System.Drawing.Size(200, 20)
$fNameLabel.Text = "User's First Name"
$fNameLabel.Font = $buttonFont

$fNameInput = New-Object System.Windows.Forms.TextBox
$fNameInput.Location = New-Object System.Drawing.Size(240, 95)
$fNameInput.Size = New-Object System.Drawing.Size(200, 50)
$fNameInput.Font = $buttonFont

$lNameLabel = New-Object System.Windows.Forms.Label
$lNameLabel.Location = New-Object System.Drawing.Size(460, 75)
$lNameLabel.Size = New-Object System.Drawing.Size(200, 20)
$lNameLabel.Text = "User's Last Name"
$lNameLabel.Font = $buttonFont

$lNameInput = New-Object System.Windows.Forms.TextBox
$lNameInput.Location = New-Object System.Drawing.Size(460, 95)
$lNameInput.Size = New-Object System.Drawing.Size(200, 50)
$lNameInput.Font = $buttonFont

$restartButton = New-Object System.Windows.Forms.Button
$restartButton.Location = New-Object System.Drawing.Size(0,0)
$restartButton.Size = New-Object System.Drawing.Size(30,30)
$restartButton.Text = [char]::ConvertFromUtf32(80)
$restartButton.Font = New-Object System.Drawing.Font("Wingdings 3",12,[System.Drawing.FontStyle]::Regular)
$restartButton.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
$restartButton.FlatAppearance.BorderColor = [System.Drawing.Color]::WhiteSmoke
$restartButton.Add_Click({
    $main_form.Close()
    $main_form.Dispose()
    Invoke-Expression "& '$($GenesisLocation)\Scripts\buildGUI.ps1'"
})

$restartExplorerButton = New-Object System.Windows.Forms.Button
$restartExplorerButton.Location = New-Object System.Drawing.Size(20,20)
$restartExplorerButton.Size = $buttonSize
$restartExplorerButton.Text = "Restart Windows Explorer"
$restartExplorerButton.Font = $buttonFont
$restartExplorerButton.Add_Click( {
    $restartExplorerButton.Text = "Restarting Windows Explorer..."
    $restartExplorerButton.BackColor = $inProgressColor
    Invoke-Expression "& '$($GenesisLocation)\Scripts\restartExplorer.ps1'"
    $restartExplorerButton.Text = "Restart Windows Explorer"
    $restartExplorerButton.BackColor = $completeColor
} )

$installButton = New-Object System.Windows.Forms.Button
$installButton.Location = New-Object System.Drawing.Size(220,20)
$installButton.Size = $buttonSize
$installButton.Text = "Install Genesis Locally"
$installButton.Font = $buttonFont
$installButton.Add_Click( {
    $installButton.Text = "Installing Genesis..."
    $installButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\installer.ps1'"
    $installButton.Text = "Install Genesis Locally"
    $installButton.BackColor = $completeColor
} )

$updateButton = New-Object System.Windows.Forms.Button
$updateButton.Location = New-Object System.Drawing.Size(420,20)
$updateButton.Size = $buttonSize
$updateButton.Text = "Update Genesis"
$updateButton.Font = $buttonFont
$updateButton.Add_Click( {
    $updateButton.Text = "Updating Genesis..."
    $updateButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\update.ps1'"
    $updateButton.Text = "Update Genesis"
    $updateButton.BackColor = $completeColor
} )

$setup1MGButton = New-Object System.Windows.Forms.Button
$setup1MGButton.Location = New-Object System.Drawing.Size(20,70)
$setup1MGButton.Size = $buttonSize
$setup1MGButton.Text = "Setup 1 MG"
$setup1MGButton.Font = $buttonFont
$setup1MGButton.Add_Click( {
    $setup1MGButton.Text = "Setting up 1 MG..."
    $setup1MGButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\setup1MG.ps1'"
    $setup1MGButton.Text = "Setup 1 MG"
    $setup1MGButton.BackColor = $completeColor
} )

$MGTweaksButton = New-Object System.Windows.Forms.Button
$MGTweaksButton.Location = New-Object System.Drawing.Size(220,70)
$MGTweaksButton.Size = $buttonSize
$MGTweaksButton.Text = "MG Tweaks"
$MGTweaksButton.Font = $buttonFont
$MGTweaksButton.Add_Click( {
    $MGTweaksButton.Text = "Running MG Tweaks..."
    $MGTweaksButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\mgTweaks.ps1'"
    $MGTweaksButton.Text = "MG Tweaks"
    $MGTweaksButton.BackColor = $completeColor
} )

$bloatwareButton = New-Object System.Windows.Forms.Button
$bloatwareButton.Location = New-Object System.Drawing.Size(420,70)
$bloatwareButton.Size = $buttonSize
$bloatwareButton.Text = "Remove Bloatware"
$bloatwareButton.Font = $buttonFont
$bloatwareButton.Add_Click( {
    $bloatwareButton.Text = "Removing Bloatware..."
    $bloatwareButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\removeBloatware.ps1'"
    $bloatwareButton.Text = "Remove Bloatware"
    $bloatwareButton.BackColor = $completeColor
} )

$installProgramsButton = New-Object System.Windows.Forms.Button
$installProgramsButton.Location = New-Object System.Drawing.Size(20,120)
$installProgramsButton.Size = $buttonSize
$installProgramsButton.Text = "Install Programs"
$installProgramsButton.Font = $buttonFont
$installProgramsButton.Add_Click( {
    $installProgramsButton.Text = "Installing Programs..."
    $installProgramsButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\installPrograms.ps1' -builtByMG $($builtByMGCheckBox.Checked)"
    $installProgramsButton.Text = "Install Programs"
    $installProgramsButton.BackColor = $completeColor
} )

$cleanFilesButton = New-Object System.Windows.Forms.Button
$cleanFilesButton.Location = New-Object System.Drawing.Size(220,120)
$cleanFilesButton.Size = $buttonSize
$cleanFilesButton.Text = "Clean Files"
$cleanFilesButton.Font = $buttonFont
$cleanFilesButton.Add_Click( {
    $cleanFilesButton.Text = "Cleaning Files..."
    $cleanFilesButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\cleanFiles.ps1'"
    $cleanFilesButton.Text = "Clean Files"
    $cleanFilesButton.BackColor = $completeColor
} )

$scheduleDefragButton = New-Object System.Windows.Forms.Button
$scheduleDefragButton.Location = New-Object System.Drawing.Size(420,120)
$scheduleDefragButton.Size = $buttonSize
$scheduleDefragButton.Text = "Schedule Defrag"
$scheduleDefragButton.Font = $buttonFont
$scheduleDefragButton.Add_Click( {
    $scheduleDefragButton.Text = "Scheduling Defrag..."
    $scheduleDefragButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\scheduleDefrag.ps1'"
    $scheduleDefragButton.Text = "Schedule Defrag"
    $scheduleDefragButton.BackColor = $completeColor
} )

$repairVolumeButton = New-Object System.Windows.Forms.Button
$repairVolumeButton.Location = New-Object System.Drawing.Size(20,170)
$repairVolumeButton.Size = $buttonSize
$repairVolumeButton.Text = "Repair Volume"
$repairVolumeButton.Font = $buttonFont
$repairVolumeButton.Add_Click( {
    $repairVolumeButton.Text = "Repairing Volume..."
    $repairVolumeButton.BackColor = $inProgressColor
    Repair-Volume -DriveLetter c
    $repairVolumeButton.Text = "Repair Volume"
    $repairVolumeButton.BackColor = $completeColor
} )

$windowsUpdatesButton = New-Object System.Windows.Forms.Button
$windowsUpdatesButton.Location = New-Object System.Drawing.Size(220,170)
$windowsUpdatesButton.Size = $buttonSize
$windowsUpdatesButton.Text = "Windows Updates"
$windowsUpdatesButton.Font = $buttonFont
$windowsUpdatesButton.Add_Click( {
    $windowsUpdatesButton.Text = "Installing Windows Updates..."
    $windowsUpdatesButton.BackColor = $inProgressColor
    Invoke-Expression "& 'C:\1 MG\Genesis\Scripts\softwareUpdates.ps1'"
    $windowsUpdatesButton.Text = "Windows Updates"
    $windowsUpdatesButton.BackColor = $completeColor
} )

$sfcButton = New-Object System.Windows.Forms.Button
$sfcButton.Location = New-Object System.Drawing.Size(420,170)
$sfcButton.Size = $buttonSize
$sfcButton.Text = "Run SFC Scan"
$sfcButton.Font = $buttonFont
$sfcButton.Add_Click( {
    $sfcButton.Text = "Running SFC Scan..."
    $sfcButton.BackColor = $inProgressColor
    Invoke-Expression "& '$($GenesisLocation)\Scripts\sfc.ps1'"
    $sfcButton.Text = "Run SFC Scan"
    $sfcButton.BackColor = $completeColor
} )

$changeBGButton = New-Object System.Windows.Forms.Button
$changeBGButton.Location = New-Object System.Drawing.Size(20,220)
$changeBGButton.Size = $buttonSize
$changeBGButton.Text = "Change Background Image"
$changeBGButton.Font = $buttonFont
$changeBGButton.Add_Click( {
    $changeBGButton.Text = "Changing Background Image..."
    $changeBGButton.BackColor = $inProgressColor
    Invoke-Expression "& '$($GenesisLocation)\Scripts\updateBG.ps1'"
    $changeBGButton.Text = "Change Background Image"
    $changeBGButton.BackColor = $completeColor
} )

$updateOEMButton = New-Object System.Windows.Forms.Button
$updateOEMButton.Location = New-Object System.Drawing.Size(220,220)
$updateOEMButton.Size = $buttonSize
$updateOEMButton.Text = "Update OEM Info"
$updateOEMButton.Font = $buttonFont
$updateOEMButton.Add_Click( {
    $updateOEMButton.Text = "Updating OEM Info..."
    $updateOEMButton.BackColor = $inProgressColor
    Invoke-Expression "& '$($GenesisLocation)\Scripts\updateOEM.ps1'"
    $updateOEMButton.Text = "Update OEM Info"
    $updateOEMButton.BackColor = $completeColor
} )

$installToFlashDriveButton = New-Object System.Windows.Forms.Button
$installToFlashDriveButton.Location = New-Object System.Drawing.Size(420,220)
$installToFlashDriveButton.Size = $buttonSize
$installToFlashDriveButton.Text = "Install Genesis to Flash Drive"
$installToFlashDriveButton.Font = $buttonFont
$installToFlashDriveButton.Add_Click( {
    $installToFlashDriveButton.Text = "Installing Genesis to Flash Drive..."
    $installToFlashDriveButton.BackColor = $inProgressColor
    $flashDrive_form = New-Object System.Windows.Forms.Form
    $flashDrive_form.Text ='Install To Flash Drive'
    $flashDrive_form.AutoSize = $true

    $flashButtonY = 0;

    foreach ($drive in get-volume) {
        if ($drive.DriveLetter -and $drive.DriveLetter -ne "C" -and $drive.FileSystemLabel) {
            $driveButton = New-Object System.Windows.Forms.Button
            $driveButton.Location = New-Object System.Drawing.Size(0,$flashButtonY)
            $driveButton.Size = $buttonSize
            $driveButton.Font = $buttonFont
            if (Get-Item -Path "$($drive.DriveLetter):\Genesis" -Force -ErrorAction SilentlyContinue) {
                $driveButton.Text = "Installed on $($drive.DriveLetter) - $($drive.FileSystemLabel)"
                $driveButton.BackColor = $completeColor
            } else {
                $driveButton.Text = "$($drive.DriveLetter) - $($drive.FileSystemLabel)"
                $driveButton.Add_Click({
                    $inProgressColor = [System.Drawing.Color]::LightYellow
                    $completeColor = [System.Drawing.Color]::LightGreen
                    $driveButton.Text = "Installing to $($drive.DriveLetter)..."
                    $driveButton.BackColor = $inProgressColor
                    Invoke-Expression "& '$($GenesisLocation)\Scripts\installToFlashDrive.ps1' -driveLetter $($drive.DriveLetter)"
                    $driveButton.Text = "Installed on $($drive.DriveLetter) - $($drive.FileSystemLabel)"
                    $driveButton.BackColor = $completeColor
                    $driveButton.Remove_Click({})
                }.GetNewClosure())
            }
            $flashDrive_form.Controls.Add($driveButton)
            $flashButtonY += 50
        }
    }

    $flashDrive_form.ShowDialog()

    $installToFlashDriveButton.Text = "Install Genesis to Flash Drive"
    $installToFlashDriveButton.BackColor = $completeColor
} )

$buttonsGroup.Controls.Add($runButton)
$buttonsGroup.Controls.Add($builtByMGCheckBox)
$buttonsGroup.Controls.Add($fNameLabel)
$buttonsGroup.Controls.Add($fNameInput)
$buttonsGroup.Controls.Add($lNameLabel)
$buttonsGroup.Controls.Add($lNameInput)

$advancedButtons.Controls.Add($installButton)
$advancedButtons.Controls.Add($updateButton)
$advancedButtons.Controls.Add($setup1MGButton)
$advancedButtons.Controls.Add($MGTweaksButton)
$advancedButtons.Controls.Add($bloatwareButton)
$advancedButtons.Controls.Add($installProgramsButton)
$advancedButtons.Controls.Add($cleanFilesButton)
$advancedButtons.Controls.Add($scheduleDefragButton)
$advancedButtons.Controls.Add($repairVolumeButton)
$advancedButtons.Controls.Add($windowsUpdatesButton)
$advancedButtons.Controls.Add($sfcButton)
$advancedButtons.Controls.Add($restartExplorerButton)
$advancedButtons.Controls.Add($changeBGButton)
$advancedButtons.Controls.Add($updateOEMButton)
$advancedButtons.Controls.Add($installToFlashDriveButton)

$buttonsGroup.Controls.Add($advancedButtons)
$main_form.Controls.Add($buttonsGroup)
$main_form.Controls.Add($logoBox)
$main_form.Controls.Add($restartButton)

Write-Host "GUI Ready!" -ForegroundColor Green
$main_form.ShowDialog()