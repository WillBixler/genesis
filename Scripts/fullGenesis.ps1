
Param(
    $builtByMG,
    $uName
)

Clear-Host
Write-Host "Checking Genesis Location" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\installer.ps1'"

Clear-Host
Write-Host "Checking Genesis Version" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\update.ps1'"

Clear-Host
Write-Host "Setting up 1 MG" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\setup1MG.ps1'"

if ($builtByMG -eq $true) {

    Write-Host "Changing Background" -ForegroundColor Yellow
    Invoke-Expression "& '$($GenesisLocation)\Scripts\updateBG.ps1'"
    Write-Host "Changing Background" -ForegroundColor Yellow

    Write-Host "Updating OEM Info" -ForegroundColor Yellow
    Invoke-Expression "& '$($GenesisLocation)\Scripts\updateOEM.ps1'"
    Write-Host "Done Updating OEM Info" -ForegroundColor Yellow

}

Clear-Host
Write-Host "MG Tweaks" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\mgTweaks.ps1' -uName '$($uName)'"
Write-Host "MG Tweaks Done" -ForegroundColor Yellow

Clear-Host
Write-Host "Removing Bloatware" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\RemoveBloatware.ps1'"
Write-Host "Done Removing Bloatware" -ForegroundColor Yellow

Clear-Host
Write-Host "Installing Programs" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\installPrograms.ps1' -builtByMG $($builtByMG)"
Write-Host "Done Installing Programs" -ForegroundColor Yellow

Clear-Host
Write-Host "Cleaning Files" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\CleanFiles.ps1'"
Write-Host "Done Cleaning" -ForegroundColor Yellow

Clear-Host
Write-Host "Scheduling Defrag" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\ScheduleDefrag.ps1'"
Write-Host "Done Scheduling Defrag" -ForegroundColor Yellow

Clear-Host
Write-Host "Repairing Volume" -ForegroundColor Yellow
Repair-Volume -DriveLetter c
Write-Host "Done Repairing Volume" -ForegroundColor Yellow

Clear-Host
Write-Host "Looking for software updates" -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\softwareUpdates.ps1'"
Write-Host "Done updating software" -ForegroundColor Yellow

Clear-Host
Write-Host "SFC scan... This can take a while..." -ForegroundColor Yellow
Invoke-Expression "& '$($GenesisLocation)\Scripts\sfc.ps1'"
Write-Host "Finished SFC scan" -ForegroundColor Yellow

Clear-Host
Write-Host "Full Genesis Done!" -ForegroundColor Green
