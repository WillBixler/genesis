
function ShutdownPrograms {

    StopProgram -name chrome
    StopProgram -name iexplore
    StopProgram -name microsoftedge
    StopProgram -name firefox
    
}

function StopProgram {
    param (
        $name
    )
    try {
        Stop-Process -Name $name -ErrorAction Stop
        Write-Host "Shut down $($name)" -ForegroundColor Green
    } catch {
        Write-Host "Could not shut down $($name): $($Error[0])" -ForegroundColor Red
    }
}

Clear-Host

# Empty Recycle Bin
Write-Output "Emptying the recycle bin"
Clear-RecycleBin -Force

ShutdownPrograms

if ((Get-Location).Drive.Name -eq "C") {
    $definitionsFileLocation = "/1 MG/Genesis/Definitions/Temp Files.json"
} else {
    $definitionsFileLocation = "/Genesis/Definitions/Temp Files.json"
}

$rawDefinitions = Get-Content -Raw -Path $definitionsFileLocation
$definitions = @{}
(ConvertFrom-Json $rawDefinitions).psobject.properties | ForEach-Object { $definitions[$_.Name] = $_.Value }

foreach($key in $definitions.Keys) {
    
    foreach($item in $definitions[$key]) {
        $item = $item -replace "\(LOCALAPPDATA\)" , $env:LOCALAPPDATA
        $item = $item -replace "\(APPDATA\)", $env:APPDATA
        try {
            Remove-Item -Path $item -Recurse -Force -ErrorAction SilentlyContinue
            Write-Host "Removed $($item)" -ForegroundColor Green
        } catch {
            Write-Host "Could not remove $($item): $($Error[0])" -ForegroundColor Red
        }
    }
}

Write-Host "Done Cleaning Files!" -ForegroundColor Green