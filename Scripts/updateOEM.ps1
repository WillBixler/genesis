
Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name Logo -Value "C:\1 MG\Genesis\Resources\MG Logo.bmp"
Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name Manufacturer -Value "Millennium Group"
Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name SupportHours -Value "9am - 5pm"
Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name SupportPhone -Value "970-663-1200"
Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name SupportURL -Value "https://www.mghelpme.com"

$processor = get-wmiobject win32_processor | select Name

if ($processor -like "*Ryzen 3*") {
    Write-Host "Detected ION" -ForegroundColor Yellow
    Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name Model -Value "Ion"
} elseif ($processor -like "*Ryzen 5*") {
    Write-Host "Detected GRAVITON" -ForegroundColor Yellow
    Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name Model -Value "Graviton"
} elseif ($processor -like "*Ryzen 7*") {
    Write-Host "Detected TACHYON" -ForegroundColor Yellow
    Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name Model -Value "Tachyon"
} elseif ($processor -like "*Athalon*") {
    Write-Host "Detected PREON" -ForegroundColor Yellow
    Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation -Name Model -Value "Preon"
}