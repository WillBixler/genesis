
function CheckInternetConnection {
    return Test-Connection "www.mghelpme.com" -Count 1 -Quiet
}

function CreateSettingsFile {

    $Settings = New-Object -TypeName psobject

    $Settings | Add-Member -MemberType NoteProperty -Name "runMode" -Value "production"

    $Settings | ConvertTo-Json | Out-File -FilePath "Settings.json"

}

function GitUpdate {
    
    try {
        Set-Location "C:/1 MG/Genesis" -ErrorAction Stop
        Write-Host "Updating local Genesis" -ForegroundColor Yellow
        Start-Process "PortableGit/git-cmd.exe" -ArgumentList "git add . && git reset --hard && git pull && exit" -Wait
        Write-Host "Local Genesis updated" -ForegroundColor Green
    } catch {
        # Genesis not installed
    }

}

function GitPull {
    
    try {
        Set-Location "C:/1 MG/Genesis" -ErrorAction Stop
        Write-Host "Pulling remote Genesis" -ForegroundColor Yellow
        Start-Process "PortableGit/git-cmd.exe" -ArgumentList "git add . && git pull && exit" -Wait
        Write-Host "Local Genesis merged" -ForegroundColor Green
    } catch {
        # Genesis not installed
    }

}

function UpdatePortable {

    Write-Host "Looking for Portable Genesis' for updates..." -ForegroundColor Yellow

    Get-Volume | ForEach-Object {
        # Loop through each drive to find portable Genesis
        try {
            $drive = $_.DriveLetter
            Set-Location "$($drive):\Genesis" -ErrorAction Stop
            # Genesis is in current drive
            Write-Host "Updating Portable Genesis in $($drive):" -ForegroundColor Green
            Start-Process "PortableGit\git-cmd.exe" -ArgumentList "git add . && git reset --hard && git pull && exit" -Wait
            Write-Host "Portable Update Finished" -ForegroundColor Yellow
        } catch {
            # Didnt Find Genesis in current drive
        }
    }

    Set-Location "C:\1 MG\Genesis"

}

Clear-Host

if (!(CheckInternetConnection)) {
    Write-Host "No internet connection" -ForegroundColor Red
    break
}

# Load Settings
$Settings = Get-Content "C:\1 MG\Genesis\Settings.json" -ErrorAction SilentlyContinue | ConvertFrom-Json

if ((!$Settings) -or ($Settings.runMode -ne "developer")) {
        CreateSettingsFile
        GitUpdate
        UpdatePortable
} else {
    GitPull
    UpdatePortable
}

Write-Host "Done installing updates" -ForegroundColor Green