Clear-Host

$processor = get-wmiobject win32_processor | select Name

if ($processor -like "*Ryzen 3*") {
    Write-Host "Detected ION" -ForegroundColor Yellow
    $backgroundImage = "C:\1 MG\Genesis\Resources\Background Images\Ion.jpg"
} elseif ($processor -like "*Ryzen 5*") {
    Write-Host "Detected GRAVITON" -ForegroundColor Yellow
    $backgroundImage = "C:\1 MG\Genesis\Resources\Background Images\Graviton.jpg"
} elseif ($processor -like "*Ryzen 7*") {
    Write-Host "Detected TACHYON" -ForegroundColor Yellow
    $backgroundImage = "C:\1 MG\Genesis\Resources\Background Images\Tachyon.jpg"
} elseif ($processor -like "*Athalon*") {
    Write-Host "Detected PREON" -ForegroundColor Yellow
    $backgroundImage = "C:\1 MG\Genesis\Resources\Background Images\Preon.jpg"
} else {
    Write-Host "Detected GENERIC" -ForegroundColor Yellow
    $backgroundImage = "C:\1 MG\Genesis\Resources\Background Images\General.jpg"
}

Set-ItemProperty -Path "REGISTRY::HKEY_CURRENT_USER\Control Panel\Desktop" -Name "WallPaper" -Value $backgroundImage
Set-ItemProperty -Path "REGISTRY::HKEY_USERS\.DEFAULT\Control Panel\Desktop" -Name "WallPaper" -Value $backgroundImage

RUNDLL32.EXE USER32.DLL,UpdatePerUserSystemParameters 1, True

Write-Host "Background Updated" -ForegroundColor Green