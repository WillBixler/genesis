
function CreateShortcut {
    param (
        $FileLocation,
        $ShortcutLocation
    )
    if (!(Get-Item $ShortcutLocation -ErrorAction SilentlyContinue)) {
        $WScriptShell = New-Object -ComObject WScript.Shell
        $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
        $Shortcut.TargetPath = $FileLocation
        $Shortcut.Save()
    }
}

function CreateFolderShortcut {
    param (
        $FileLocation,
        $ShortcutLocation,
        $IconLocation
    )
    
    $WScriptShell = New-Object -ComObject WScript.Shell
    $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
    $Shortcut.TargetPath = $FileLocation
    if ($IconLocation) {
        $Shortcut.IconLocation = $IconLocation
    }
    $Shortcut.Save()
}

Clear-Host

# Create 1 MG Desktop Shortcut
Write-Host "Creating 1 MG Shortcut" -ForegroundColor Yellow
CreateFolderShortcut -FileLocation "C:\1 MG" -ShortcutLocation "C:\Users\$($env:Username)\Desktop\1 MG.lnk" -IconLocation "C:\1 MG\Genesis\Resources\MG Icon.ico"

# Create Shortcuts
Write-Host "Creating Shortcuts" -ForegroundColor Yellow
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\CCleaner64.exe" -ShortcutLocation "C:\1 MG\CCleaner.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\Defraggler64.exe" -ShortcutLocation "C:\1 MG\Defraggler.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\Speccy64.exe" -ShortcutLocation "C:\1 MG\Speccy.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\HDD Scan\HDDScan.exe" -ShortcutLocation "C:\1 MG\HDD Scan.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\TeamViewer_Host_Setup.exe" -ShortcutLocation "C:\1 MG\Teamviewer Host Setup.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\FirefoxPortable\FirefoxPortable.exe" -ShortcutLocation "C:\1 MG\Firefox Portable.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Tools\Autoruns\autoruns.exe" -ShortcutLocation "C:\1 MG\Autoruns.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Resources\Ninite SAS.exe" -ShortcutLocation "C:\1 MG\Install SAS.lnk"
CreateShortcut -FileLocation "C:\1 MG\Genesis\Resources\Install Vipre.exe" -ShortcutLocation "C:\1 MG\Install Vipre.lnk"
CreateFolderShortcut -FileLocation "C:\1 MG\Genesis\Tools\Snappy\" -ShortcutLocation "C:\1 MG\Snappy Drivers.lnk"
CreateFolderShortcut -FileLocation "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" -ShortcutLocation "C:\1 MG\Start Menu (Windows 10).lnk" -IconLocation "C:\WINDOWS\system32\imageres.dll, 76"
CreateFolderShortcut -FileLocation "C:\1 MG\Genesis\Resources\Background Images" -ShortcutLocation "C:\1 MG\Background Images.lnk"

# Remove old 1 MG Applications
Write-Host "Removing old 1 MG" -ForegroundColor Yellow
Remove-Item -Path "C:\1 MG\App" -Recurse -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Data" -Recurse -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Other" -Recurse -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Vipre kit" -Recurse -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\CCleaner.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\CCleaner64.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Defraggler.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Defraggler64.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\FirefoxPortable.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\mbam.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Speccy.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\Speccy64.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\1 MG\StartMenu (vista-7).lnk" -Force -ErrorAction SilentlyContinue

# Create Setup Info.txt if it doesn't exist
Write-Host "Creating Setup Info.txt" -ForegroundColor Yellow
if (!(Get-Item "C:\1 MG\Setup Info.txt" -ErrorAction SilentlyContinue)) {
    New-Item "C:\1 MG\Setup Info.txt"
}

# Hide 1 MG Folder
(Get-Item "C:\1 MG" -Force).Attributes = "Hidden"

Write-Host "Setup Done!" -ForegroundColor Green