
Clear-Host

#Define update criteria.
$Criteria = "IsInstalled=0"

#Search for relevant updates.
Write-Host "Checking for Windows updates"
$Searcher = New-Object -ComObject Microsoft.Update.Searcher
$SearchResult = $Searcher.Search($Criteria).Updates

if ($SearchResult.Count -gt 0) {
    Write-Host "Found $($SearchResult.Count) Updates"

    #Download updates.
    Write-Host "Downloading Windows updates" -ForegroundColor Yellow
    $Session = New-Object -ComObject Microsoft.Update.Session
    $Downloader = $Session.CreateUpdateDownloader()
    $Downloader.Updates = $SearchResult
    $Downloader.Download()

    Write-Host "Installing Windows updates" -ForegroundColor Yellow
    $Installer = New-Object -ComObject Microsoft.Update.Installer
    $Installer.Updates = $SearchResult
    $Result = $Installer.Install()
} else {
    Write-Host "No Windows updates at this time."
}

Write-Host "Done installing updates" -ForegroundColor Green