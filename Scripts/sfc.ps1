Clear-Host
Write-Host "Running SFC Scan... This can take a while" -ForegroundColor Yellow
sfc /scannow
Write-Host "SFC Scan Done!" -ForegroundColor Green