# Title:            Remove Bloatware
# Author:           Millennium Group
# Contributors:     Will Bixler
# Bugs:             None
#
# Description:      Software autonomously removes Windows bloatware

function GetPackage {
    param (
        $packageName
    )
    Get-AppxPackage *$packageName* | Where-Object {$ignoreList -notcontains $_.name}
}

function Get-InstalledApps {
    # For each name in bloatware list
    foreach ($appName in $bloatware) {
        # Get the app Package
        $app = GetPackage $appName
        # If the app was found and is installed
        if ($app) {
            Write-Host "$($appName) is installed" -ForegroundColor Green
        } else {
            Write-Host "$($appName) not installed" -ForegroundColor Red
            [void]$notInstalled.Add($appName)
        }
    }

    # For each app in the not installed list
    foreach($app in $notInstalled){
        # Remove from the bloatware list
        $bloatware.Remove($app)
    }

    # Reset not installed list
    $notInstalled = @()
}

function Show-Queue {
    Write-Output "The following applications will be removed:"
    # for each item in bloatware list
    foreach ($item in $bloatware) {
        # Check if item is in the keep list
        if (!$keepList.Contains($item)) {
            # Write item
            Write-Host $item -ForegroundColor Red
        }
    }
}

function Customize-Queue {
    Write-Output "Queued for Removal"
    # while bloatware has items
    for ($i = 0; $i -lt $bloatware.Count; $i++) {
        # Check if item is in keep list
        if (!$keepList.Contains($bloatware[$i])) {
            # Write item with index
            Write-Host "$($i) $($bloatware[$i])" -ForegroundColor Red
        }
    }
}

function Remove-InstalledApps {
    # for each item in bloatware list
    foreach ($app in $bloatware) {
        # check if in keep list
        if (!$keepList.Contains($app)) {
            # write removing item in red
            Write-Host "Removing $($app)" -ForegroundColor Red
            # Get item package and remove for local user
            GetPackage $app | Remove-AppxPackage

            # If package still exists
            if (GetPackage $app) {
                # Remove package for all users
                GetPackage $app | Remove-AppxPackage -AllUsers
            }
        } else {
            # write skipping item in green
            Write-Host "Skipping $($app)" -ForegroundColor Green
        }
    }
}

Clear-Host

Write-Host "Setting Definitions" -ForegroundColor Yellow

if ((Get-Location).Drive.Name -eq "C") {
    $definitionsFileLocation = "/1 MG/Genesis/Definitions/Bloatware.json"
} else {
    $definitionsFileLocation = "/Genesis/Definitions/Bloatware.json"
}

$definitions = Get-Content -Raw -Path $definitionsFileLocation -ErrorAction Stop | ConvertFrom-Json

[System.Collections.ArrayList]$bloatware = $definitions.bloatware
[System.Collections.ArrayList]$notInstalled = @()
[System.Collections.ArrayList]$keepList = @()
[System.Collections.ArrayList]$ignoreList = $definitions.ignoreList

# production - developer - silent
$mode = "silent"
$ProgressPreference = "SilentlyContinue"

Write-Host "Finding installed bloatware" -ForegroundColor Yellow
Get-InstalledApps

if ($mode -eq "developer") {
    Pause
}

# If no bloatware installed
if ($bloatware.Count -eq 0) {
    Write-Host "No bloatware installed" -ForegroundColor Green
    if ($mode -ne "silent") {
        Pause
    }
    exit
}

if ($mode -eq "developer") {
    do {

        # Main developer menu
        Clear-Host
        Show-Queue     

        Write-Output "What would you like to do?"
        Write-Output "1 Remove Programs"
        Write-Output "2 Edit List"
        Write-Output "3 Exit"

        $continue = Read-Host

        if ($continue -eq "2") {

            do {
                
                # Custom list developer menu
                Clear-Host

                Customize-Queue

                $escape = Read-Host -Prompt "Enter the number of the item you want to keep (continue to exit)"

                if ($escape -eq "exit") {   # Exit if user types exit
                    exit
                } elseif ($escape -ne "continue") {     # If user did not type continue
                    # Add item with index of user input to the keep list
                    [void]$keepList.Add($bloatware[$escape])
                }

            } while ($escape -ne "continue")    # Loop until the user types continue
            
        } elseif ($continue -eq "3") {  # If user typed 2
            # Exit
            Write-Output "Exiting"
            exit
        }

    } while ($continue -ne "1")     # Loop until the user types 1
}

# While bloatware has items with a maximum of 3 iterations
for ($i = 0; $i -lt 3 -and $bloatware.Count -gt 0; $i++){
    # Remove apps
    Write-Host "Removing Bloatware" -ForegroundColor Yellow
    Remove-InstalledApps

    # Verify Removal
    Write-Host "Verifying Status" -ForegroundColor Yellow
    Get-InstalledApps
}

if ($bloatware.Count -gt 0) {       # Apps still installed
    Write-Host "Some apps could not be removed" -ForegroundColor Yellow
    Get-InstalledApps
} else {        # All apps were removed
    Write-Host "All apps were removed successfully" -ForegroundColor Yellow
}
if ($mode -ne "silent") {
    Pause
}

Write-Host "Done Removing Bloatware" -ForegroundColor Green