
Clear-Host

# Start defrag in background
Optimize-Volume C -AsJob

if (!(Get-ScheduledJob -Name "MG Weekly Defrag" -ErrorAction SilentlyContinue)) {
    Write-Host "Scheduling weekly drive optimization" -ForegroundColor Yellow

    $action = { Optimize-Volume -DriveLetter C }
    $trigger = New-JobTrigger -Weekly -DaysOfWeek Saturday -At 1am -WeeksInterval 1
    $taskName = "MG Weekly Defrag"

    Write-Host "Scheduling weekly drive optimization" -ForegroundColor Yellow
    Register-ScheduledJob -Name $taskName -Trigger $trigger -ScriptBlock $action
}

Write-Host "Done scheduling defrag... Defragging in the background." -ForegroundColor Green