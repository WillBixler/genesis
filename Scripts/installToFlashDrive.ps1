Param(
    $driveLetter
)

Clear-Host

$destination = "$($driveLetter):\Genesis"

if (Get-Item $destination -ErrorAction SilentlyContinue) {
    Write-Host "Genesis already installed on drive" -ForegroundColor Yellow
} else {
    Write-Host "Installing Genesis to $($driveLetter)... This can take a few minutes..." -ForegroundColor Yellow
    Copy-Item -Path $GenesisLocation -Destination $destination -Recurse
}

Write-Host "Done installing" -ForegroundColor Green