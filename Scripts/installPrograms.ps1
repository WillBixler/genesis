Param (
    $builtByMG
)

Clear-Host
Write-Host "Installing Programs" -ForegroundColor Yellow
if ($builtByMG -eq $true) {
    Write-Host "$($builtByMG)" -ForegroundColor Yellow
    Start-Process -FilePath "C:\1 MG\Genesis\Resources\Ninite MG.exe"
} else {
    Write-Host "$($builtByMG)"
    Start-Process -FilePath "C:\1 MG\Genesis\Resources\Ninite.exe"
}
Write-Host "Done installing programs... Please see the popup dialog." -ForegroundColor Green