Clear-Host
Write-Host "Restarting Windows Explorer..." -ForegroundColor Yellow
Stop-Process -Name Explorer

Write-Host "Explorer Restarted" -ForegroundColor Green