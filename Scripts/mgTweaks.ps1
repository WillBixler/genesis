Param {
    $uName
}

Clear-Host

# Set UAC to 3rd rung
Write-Host "Updating UAC Settings" -ForegroundColor Yellow
Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System -Name ConsentPromptBehaviorAdmin -Value 5

# Setup Desktop Icons
Write-Host "Organizing Desktop" -ForegroundColor Yellow
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\Shell\Bags\1\Desktop -Name FFlags -Value 1075839525

# Show Hidden Desktop Icons (This PC, Network, User)
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value 0
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value 0
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value 0

# Pin Programs to taskbar

# Adjust Sleep Settings
Write-Host "Updating power settings" -ForegroundColor Yellow
powercfg.exe /X standby-timeout-ac 0
powercfg.exe /X standby-timeout-dc 0

# Disable Cortana
Write-Host "Disabling Cortana" -ForegroundColor Yellow
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search -Name SearchboxTaskbarMode -Value 0

# Disable Cortana Search Online
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search -Name BingSearchEnabled -Value 0
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search -Name AllowSearchToUseLocation -Value 0
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search -Name CortanaConsent -Value 0
Stop-Process -ProcessName SearchUI -ErrorAction SilentlyContinue

# Disable Animations
Write-Host "Updating Animation Settings" -ForegroundColor Yellow
# Animation updates applied on computer restart
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects -Name VisualFXSetting -Value 3
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name TaskbarAnimations -Value 0
Set-ItemProperty -Path "REGISTRY::HKEY_CURRENT_USER\Control Panel\Desktop\WindowMetrics" -Name MinAnimate -Value 0
Set-ItemProperty -Path "REGISTRY::HKEY_CURRENT_USER\Control Panel\Desktop" -Name UserPreferencesMask -Value ([byte[]](0x98,0x32,0x07,0x80,0x10,0x00,0x00,0x00))

# Set Light Color Theme
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 1

# Rename PC
$computerName = get-ItemPropertyValue REGISTRY::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName -Name ComputerName

if (($computerName -like "*desktop*" -or $computerName -like "*laptop*") -and $uName) {
    # Update computer name
    <# ------ Section Commented out due to computer killing bug ---------
    #set-ItemProperty REGISTRY::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName -Name ComputerName -Value "$($fName)$($lName)"
    #>
}

# Rename Drive
if ((get-volume -DriveLetter C).FileSystemLabel -ne "SYSTEM") {
    Write-Host "Renaming C Drive" -ForegroundColor Yellow
    Set-Volume -DriveLetter C -NewFileSystemLabel "SYSTEM"
}

# Taskbar Tweaks
Write-Host "Cleaning up taskbar" -ForegroundColor Yellow
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer -Name EnableAutoTray -Value 0
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People -Name PeopleBand -Value 0
Set-ItemProperty -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name ShowTaskViewButton -Value 0

# IE Tweaks
Write-Host "Updating IE Settings" -ForegroundColor Yellow
Set-ItemProperty -Path "REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\Main" -Name "Start Page" -Value "https://www.google.com"

# Chrome Tweaks 

# Organize Start Menu
Set-Location "C:\ProgramData\Microsoft\Windows\Start Menu\Programs"
if (!(Get-Item "Microsoft\" -ErrorAction SilentlyContinue)) {
    New-Item Microsoft -ItemType Directory
}

Move-Item -Path @('Access*.lnk', 'Excel*', 'InfoPath*', 'OneDrive*', 'OneNote*', 'Outlook*', 'PowerPoint*', 'Publisher*', 'Skype*', 'Word*') -Destination Microsoft -ErrorAction SilentlyContinue

# Pin Microsoft Products to Start Menu
Copy-Item -Path @("Microsoft\Outlook*.lnk", "Microsoft\Excel*.lnk", "Microsoft\Word*.lnk") -Destination "$($env:APPDATA)\ClassicShell\Pinned"

# Pin Snipping tool to Start Menu
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut("$($env:APPDATA)\ClassicShell\Pinned\Snipping Tool.lnk")
$Shortcut.TargetPath = "C:\Windows\System32\SnippingTool.exe"
$Shortcut.Save()

# Update Paint.Net, CD Burner, Libre Office

# Update 1 MG Tools

# If Quickbooks is installed, install and run Connectivity Tool

# Tweak Outlook

# Restart Windows Explorer
## Write-Host "Restarting Windows Explorer" -ForegroundColor Yellow
## Start-Process powershell -ArgumentList "Stop-Process -ProcessName explorer"

Write-Host "MG Tweaks Done!" -ForegroundColor Green