# Title:            Genesis
# Author:           Millennium Group
# Contributors:     Will Bixler
# Bugs:             None
#
# Description:      Software autonomously performs the Millennium Group's basic
#                   service to clean and optimize PCs.

# Elevate to admin
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

$ScriptPath = Split-Path $MyInvocation.InvocationName
New-Variable -Name GenesisLocation -Value $ScriptPath -Scope "Global"

Write-Host "Creating a Restore Point... This can take some time" -ForegroundColor Yellow
try {
    Checkpoint-Computer -Description "MG Pre Service" -ErrorAction Inquire
} catch {
    Write-Host "A restore point could not be created. Please manually create one and try again."
    Write-Host $Error -ForegroundColor Red
    Pause
    exit
}

Clear-Host
Invoke-Expression "& '$($GenesisLocation)\Scripts\buildGUI.ps1'"