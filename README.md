# Genesis

## Author           Millennium Group
### Contributors
- Will Bixler

## Description   
Genesis is the starting point for your computer. It cleans up files, optimizes settings, optimizes storage, removes bloatware, etc!

## Instructions
Right click "Genesis.ps1" and press "Run with PowerShell"

### Compatibility:
Windows machines with Powershell installed

### Known Bugs
- N/A